module gitlab.com/VandalTheGrey/go-sysinfo

go 1.16

require (
	github.com/elastic/go-windows v1.0.1
	github.com/joeshaw/multierror v0.0.0-20140124173710-69b34d4ec901
	github.com/pkg/errors v0.9.1
	github.com/prometheus/procfs v0.6.0
	github.com/stretchr/testify v1.7.0
	golang.org/x/sys v0.0.0-20210603125802-9665404d3644
	howett.net/plist v0.0.0-20201203080718-1454fab16a06
)
